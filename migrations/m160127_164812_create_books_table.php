<?php

use yii\db\Migration;

class m160127_164812_create_books_table extends Migration
{
    public function up()
    {
        $this->createTable('books', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100),
            'date_create' => $this->dateTime(),
            'date_update' => $this->dateTime(),
            'preview' => $this->string(255),
            'date' => $this->dateTime(),
            'author_id' => $this->integer()
        ]);

        $this->addForeignKey('author_id', 'books', 'author_id', 'authors', 'id', 'CASCADE', 'RESTRICT');

        $this->insert('books', [
            'name' => 'Clean code',
            'date_create' => '2001-02-12 00:00:00',
            'date_update' => NULL,
            'preview' => 'domains.jpg',
            'date' => '1988-12-22 00:00:00',
            'author_id' => 1
        ]);

        $this->insert('books', [
            'name' => 'Refactoring',
            'date_create' => '2004-06-02 00:00:00',
            'date_update' => NULL,
            'preview' => 'domains.jpg',
            'date' => '2003-02-22 00:00:00',
            'author_id' => 2
        ]);
    }

    public function down()
    {
        $this->dropForeignKey('author_id', 'books');
        $this->dropTable('books');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
