<?php

use yii\db\Migration;

class m160127_163353_create_authors_table extends Migration
{
    public function up()
    {
        $this->createTable('authors', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(100),
            'lastname' => $this->string(100),
        ]);

        $this->insert('authors', [
            'firstname' => 'Robert',
            'lastname' => 'Martin'
        ]);

        $this->insert('authors', [
            'firstname' => 'Martin',
            'lastname' => 'Fowler'
        ]);

    }

    public function down()
    {
        $this->dropTable('authors');
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
