<?php
namespace app\models;

use yii\helpers\ArrayHelper;

class AuthorsSearch extends Authors
{
    public static function getList()
    {
        $authors = Authors::find()->all();

        $list = ArrayHelper::map($authors,
            'id',
            function ($model, $defaultValue) {
                return $model->firstname . ' ' . $model->lastname;
            });

        return $list;
    }
}