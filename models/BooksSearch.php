<?php
/**
 * Created by PhpStorm.
 * User: brisk
 * Date: 24.01.16
 * Time: 17:59
 */

namespace app\models;


use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

class BooksSearch extends Books
{
    public $date_from;
    public $date_to;

    /**
     * @var UploadedFile
     */
    public $previewFile;

    public function rules()
    {
        return [
            [['name', 'author_id', 'date_from', 'date_to'], 'safe'],
            [['previewFile'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    public function upload()
    {
        if ($this->validate()) {
            $this->preview = uniqid() . '.' . $this->previewFile->extension;
            $this->previewFile->saveAs('uploads/' . $this->preview);
            return true;
        } else {
            return false;
        }
    }

    public function getPreviewSrc()
    {
        return '/uploads/' . $this->preview;
    }

    public function search($params)
    {
        $query = BooksSearch::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'name',
                'author'
            ]
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date
        ]);

        $dateFrom = $this->date_from ? date('Y-m-d H:i:s', strtotime($this->date_from)) : null;
        $dateTo = $this->date_to ? date('Y-m-d H:i:s', strtotime($this->date_to)) : null;

        $query
            ->andFilterWhere([
                'author_id' => $this->author_id,
            ])
            ->andFilterWhere(['>=', 'date', $dateFrom])
            ->andFilterWhere(['<=', 'date', $dateTo]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

}