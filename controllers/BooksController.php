<?php
namespace app\controllers;


use app\models\Books;
use app\models\BooksSearch;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\UploadedFile;

class BooksController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['list', 'edit', 'delete'],
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    public function actionList($id = null)
    {
        $searchModel = New BooksSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($id) {
            $booksModel = BooksSearch::findOne($id);
            return $this->render('list', [
                'previewBookModel' => $booksModel,
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('list', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionEdit($id)
    {
        $model = new BooksSearch();
        /** @var Books $book */
        $book = BooksSearch::findOne(['id' => $id]);
        if ($book->load(Yii::$app->request->post()) && $model->validate()) {
            $model->previewFile = UploadedFile::getInstance($model, 'previewFile');
            if ($model->previewFile && !$model->upload()) {
                throw new Exception('Something went wrong');
            } else if ($model->previewFile) {
                $book->preview = $model->preview;
            }
            $book->date_update = date('Y-m-d H:i:s');
            $book->save();
        }

        return $this->render('edit', [
            'model' => $book,
        ]);
    }

    public function actionDelete($id)
    {
        /** @var Books $book */
        $book = Books::findOne(['id' => $id]);
        @unlink(Yii::$app->basePath . '/' . Books::UPLOADS_DIRECTORY . $book->preview);
        $book->delete();

        return $this->redirect(['list']);

    }

}