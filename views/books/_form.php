<?php

use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Books */
/* @var $form ActiveForm */
?>
<div class="form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'date_create')->widget(DatePicker::className()) ?>
    <?= $form->field($model, 'date')->widget(DatePicker::className()) ?>
    <?= $form->field($model, 'author_id')->dropDownList(\app\models\AuthorsSearch::getList(), [
        ['prompt' => 'Author...']
    ]) ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'previewFile')->fileInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- _form -->
