<?php

use app\models\AuthorsSearch;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<div class="book-search">

    <?php $form = ActiveForm::begin([
        'action' => ['books/list'],
        'method' => 'get',
    ]); ?>
    <div class="form-group">
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-6">
                    <?= $form->field($model, 'author_id')->dropDownList(AuthorsSearch::getList(), [
                        'prompt' => 'Author...',
                    ]) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'name') ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group row">
                <div class="col-md-3">
                    <?php
                    echo $form->field($model, 'date_from')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter date'],
                        'pluginOptions' => [
                            'autoclose'=>true
                        ]
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?php
                    echo $form->field($model, 'date_to')->widget(DatePicker::classname(), [
                        'options' => ['placeholder' => 'Enter date'],
                        'pluginOptions' => [
                            'autoclose'=>true
                        ]
                    ]); ?>
                </div>
                <div class="col-md-3">
                    <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
                </div>
            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
