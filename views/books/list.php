<?php

/* @var $this yii\web\View */

use yii\bootstrap\Modal;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\Pjax;

$this->title = 'Books';
$this->params['breadcrumbs'][] = $this->title;

/** @var \app\models\BooksSearch $previewBookModel */

?>

<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_search', [
        'model' => $searchModel
    ]) ?>

    <?php Pjax::begin([
        'enablePushState' => false,
        'linkSelector' => '#preview-selector a',
    ]); ?>

    <?php if (!empty($previewBookModel)): ?>
        <?php
        Modal::begin([
            'id' => 'books-preview',
            'header' => "<h2>{$previewBookModel->name}</h2>",
        ]);
        ?>
        <?php
        echo DetailView::widget([
            'model' => $previewBookModel,
            'attributes' => [
                'id',
                'name',
                [
                    'attribute' => 'image',
                    'value' => $previewBookModel->getPreviewSrc(),
                    'format' => ['image', ['style' => 'max-width: 100px']],
                ],
                [
                    'attribute' => 'released_at',
                    'value' => Yii::$app->formatter->asDatetime($previewBookModel->date, 'd MMMM yyyy'),
                ],
                'author.fullname',
                'date_create:datetime',
                'date_update:datetime',
            ],
        ]);
        ?>
        <?php Modal::end(); ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                $('#books-preview').modal()
            });
        </script>
    <?php endif; ?>

    <?= $this->render('_fancybox') ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            [
                'format' => 'html',
                'value' => function ($data) {
                    /** @var \app\models\BooksSearch $data */
                    return Html::a(Html::img($data->getPreviewSrc(), ['width' => '100px']), $data->getPreviewSrc(), ['class' => 'fancybox']);
                },
            ],
            [
                'format' => 'text',
                'value' => function ($data) {
                    /** @var \app\models\Books $data */
                    return $data->author->firstname . ' ' . $data->author->lastname;
                }
            ],
            'date:datetime',
            'date_create:datetime',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{edit_action} {view_action} {delete}',
                'buttons' => [
                    'edit_action' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-edit"></span>',
                            ['books/edit', 'id' => $model->id],
                            [
                                'title' => Yii::t('app', 'edit'),
                                'target' => '_blank'
                            ]);
                    },
                    'view_action' => function ($url, $model) {
                        return Html::tag('span',
                            Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                ['books/list', 'id' => $model->id]
                            ),
                            [
                                'title' => Yii::t('app', 'preview'),
                                'id' => 'preview-selector'
                            ]
                        );
                    },
                ],
            ],
        ],
    ]) ?>
    <?php Pjax::end(); ?>
</div>

